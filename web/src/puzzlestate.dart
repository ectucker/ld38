part of game;

class PuzzleState extends State {

  SpriteBatch batch;
  Camera2D fullscreen;
  List<StreamSubscription> subscriptions = [];

  EditableTilemap map;
  EditableTileLayer meta;

  Ball ball;

  bool submitted = false;
  double countdown = 0.0;

  Vector2 tilePos = new Vector2.zero();

  String selectedName = null;
  Texture selected;

  int number;

  int maxTiles;
  double tileSize;
  double paletteHeight;

  bool resized = false;
  Vector2 centerOffset = new Vector2.zero();

  bool won = false;

  Music music;

  PuzzleState(this.map, this.music, this.number): super() {

  }

  @override
  create() {
    meta = map.getLayersContaining("meta").first;
    Map properties = meta.layer["properties"];

    batch = new SpriteBatch.defaultShader();
    fullscreen = new Camera2D.originBottomLeft(width, height);
    maxTiles = max(meta.width, meta.height);
    tileSize = pow2(min(width / maxTiles, (height - querySelector("#palette").clientHeight) / maxTiles));
    fullscreen.setScale(tileSize / 64, tileSize / 64);

    ball = new Ball(new Vector2(properties["startX"], properties["startY"]), atlas["ball"]);
    ball.velocity = new Vector2(properties["velocityX"], properties["velocityY"]);

    querySelector("#main").hidden = true;
    querySelector("#hud").hidden = false;
    querySelector("#levelselect").hidden = true;

    querySelector("#palette").innerHtml = "";
    String img = '<img class="tile" src="assets/img/delete.png">';
    querySelector("#palette").appendHtml(img);
    properties.forEach((String key, double value) {
      print(key);
      print(int.parse(value.toString()));
      if(!key.contains("start") && !key.contains("velocity")) {
        String img = '<img class="tile" src="assets/img/' + key + '.png">';
        for(int i = 0; i < int.parse(value.toString()); i++) {
          querySelector("#palette").appendHtml(img);
        }
      }
    });
    querySelector("#palette").children.forEach((Element element) {
      element.onClick.listen(selectTile);
    });

    selected = atlas["empty"];

    paletteHeight = querySelector("#palette").clientHeight * (64 / tileSize);

    music.loop();
  }

  selectTile(MouseEvent e) {
    ImageElement imageElement = e.target;
    if(selectedName != null && selectedName != "delete") {
      HtmlElement element = new Element.html('<img class="tile" src="assets/img/' + selectedName + '.png">');
      querySelector("#palette").append(element);
      element.onClick.listen(selectTile);
    }
    selectedName = Path.basenameWithoutExtension(imageElement.src);
    selected = atlas[selectedName];

    if(selectedName != "delete") {
      imageElement.hidden = true;
    }
    setCursor(false);
  }

  @override
  preload() {

  }

  @override
  render(num delta) {
    centerOffset.setValues(width / tileSize / 2 * 64 - 64 * meta.width / 2,
        paletteHeight + (height - paletteHeight) / tileSize / 2 * 64 - 64 * meta.height / 2);
    centerOffset.round();
    paletteHeight = querySelector("#palette").clientHeight * (64 / tileSize);

    fullscreen.update();
    batch.projection = fullscreen.combined;

    double maxDim = (max(width.toDouble(), height.toDouble()) * 2) * (64 / tileSize);
    print(maxDim);
    clearScreen(Colors.skyBlue);
    batch.begin();
    batch.draw(atlas["galaxy"], 0, 0, width: maxDim, height: maxDim);
    map.render(batch, centerOffset.x, centerOffset.y);
    ball.render(batch, centerOffset.x, centerOffset.y);
    batch.draw(selected, tilePos.x * 64 + centerOffset.x, tilePos.y * 64 + centerOffset.y);
    batch.end();
  }

  @override
  update(num delta) {
    if(querySelector("#palette").clientHeight > 32 && !resized) {
      resized = true;
      resize(width, height);
    }
    if(submitted) {
      countdown += delta;
    }
    if(countdown > 0.08 && !won) {
      if(ball.move(meta, atlas)) {
        won = true;
        HtmlElement popup = querySelector(".popup");
        popup.style.opacity = "1.0";
        completeLevels.add(number);
        music.stop();
      }
      countdown = 0.0;
    } else if(won && countdown > 1.0) {
      querySelector(".popup").style.opacity = "0";
      game.pushState(new LevelSelect());
    }
  }

  submit(KeyboardEvent e) {
    if(!submitted) {
      if (e.keyCode == KeyCode.SPACE ||
          e.keyCode == KeyCode.ENTER) {
        submitted = true;
        countdown = 0.3;
        setCursor(true);
      }
    }
    if(e.key == "r") {
      game.pushState(new PuzzleState(new EditableTilemap(map.file, atlas), music, number));
      setCursor(true);
    }
    if(e.key == "b") {
      game.pushState(new LevelSelect());
      music.stop();
      setCursor(true);
    }
  }

  highlight(MouseEvent e) {
    Vector2 screenCenter = new Vector2(width / 2 - meta.width * tileSize / 2,
        height / 2 - (meta.height / 4 * tileSize) - querySelector("#palette").clientHeight - tileSize);
    Vector2 mousePos = new Vector2(e.client.x, height - e.client.y);
    tilePos = (mousePos - new Vector2(screenCenter.x, screenCenter.y)) / tileSize;
    tilePos.round();
    if(tileSize == 32) {
      tilePos.y -= 1;
    } else if(tileSize == 16) {
      tilePos.y += 1;
    }
  }

  placeTile(MouseEvent e) {
    Vector2 screenCenter = new Vector2(width / 2 - meta.width * tileSize / 2,
        height / 2 - (meta.height / 4 * tileSize) - querySelector("#palette").clientHeight - tileSize);
    Vector2 mousePos = new Vector2(e.client.x, height - e.client.y);
    tilePos = (mousePos - new Vector2(screenCenter.x, screenCenter.y)) / tileSize;
    tilePos.round();
    if(tileSize == 32) {
      tilePos.y -= 1;
    } else if(tileSize == 16) {
      tilePos.y += 1;
    }

    if(!submitted) {
      if (selectedName != "delete") {
        setCursor(true);
        if (tilePos.x < meta.width && tilePos.x > -1 &&
            tilePos.y < meta.height &&
            tilePos.y > -1) {
          if ((meta.getTile(tilePos.x, tilePos.y) == null || meta
              .getTile(tilePos.x, tilePos.y)
              .image == "floor") && selectedName != null) {
            meta.setTile(tilePos.x, tilePos.y,
                new BasicTile(atlas[selectedName],
                    {"image": selectedName, "userplaced": true}));
            selectedName = null;
            selected = atlas["empty"];
          }
        }
      } else if (meta.getTile(tilePos.x, tilePos.y) != null && meta
          .getTile(tilePos.x, tilePos.y)
          .data
          .containsKey("userplaced")) {
        HtmlElement element = new Element.html(
            '<img class="tile" src="assets/img/' + meta
                .getTile(tilePos.x, tilePos.y)
                .image + '.png">');
        querySelector("#palette").append(element);
        element.onClick.listen(selectTile);
        meta.setTile(tilePos.x, tilePos.y,
            new BasicTile(atlas["floor"], {"image": "floor"}));
      }
    }
  }

  @override
  resize(num width, num height) {
    fullscreen = new Camera2D.originBottomLeft(width, height);
    tileSize = pow2(min(width / maxTiles, (height - querySelector("#palette").clientHeight) / maxTiles));
    fullscreen.setScale(tileSize / 64, tileSize / 64);
    paletteHeight = querySelector("#palette").clientHeight * (64 / tileSize);
    centerOffset.setValues(width / tileSize / 2 * 64 - 64 * meta.width / 2,
        paletteHeight + (height - paletteHeight) / tileSize / 2 * 64 - 64 * meta.height / 2);
    centerOffset.round();
    print(tileSize);
  }

  @override
  pause() {
    subscriptions.forEach((s) => s.cancel());
  }

  @override
  resume() {
    subscriptions.add(window.onKeyDown.listen(submit));
    subscriptions.add(window.onMouseMove.listen(highlight));
    subscriptions.add(window.onMouseUp.listen(placeTile));
  }

  double pow2(double input) {
    while(input > 0) {
      input--;
      if((input.floor() & (input.floor() - 1)) == 0) {
        return input.floorToDouble();
      }
    }
    return 1.0;
  }

  void setCursor(bool cursor) {
    document.body.style.cursor = cursor ? "auto" : "none";
  }
}