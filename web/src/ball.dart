part of game;

class Ball {
  Vector2 position;
  Vector2 velocity;

  Vector2 oldPos;
  int repeats = 10;

  Texture texture;

  List bounce;
  Sound crackIce;
  Sound breakIce;
  Sound portal;
  Sound switchFlip;
  Sound win;

  Random rand;

  Ball(this.position, this.texture) {
    velocity = new Vector2(0.0, 0.0);

    bounce = [assetManager.get("bounce2"), assetManager.get("bounce2")];
    crackIce = assetManager.get("crack");
    breakIce = assetManager.get("break");
    portal = assetManager.get("portal");
    switchFlip = assetManager.get("switch");
    win = assetManager.get("win");

    rand = new Random();
  }

  bool move(EditableTileLayer meta, Map<String, Texture> atlas) {
    if(oldPos == position) {
      repeats++;
    } else {
      repeats = 0;
    }
    oldPos = position.clone();
    if(repeats > 10) {
      return false;
    }

    BasicTile tileIn = null;
    if(position.x > 0 && position.y > 0 && position.x <= meta.width && position.y <= meta.height) {
      tileIn = meta.getTile(position.x, position.y);
    }
    if(tileIn != null && tileIn.image.contains("ramp")) {
      Vector2 cache = velocity.clone();
      velocity.x = cache.y;
      velocity.y = cache.x;
      if (tileIn.image.contains("left")) {
        velocity.x *= velocity.x.sign;
      }
      if (tileIn.image.contains("right")) {
        if (velocity.x > 0) {
          velocity.x *= -1;
        }
      }
      if (tileIn.image.contains("bottom")) {
        velocity.y *= velocity.y.sign;
      }
      if (tileIn.image.contains("top")) {
        if (velocity.y > 0) {
          velocity.y *= -1;
        }
      }
      bounce[rand.nextInt(2)].play();
    }

    Vector2 target = position + velocity;
    BasicTile tile = null;
    if(target.x >= 0 && target.y >= 0
        && target.x < meta.width && target.y < meta.height) {
      tile = meta.getTile(target.x, target.y);
      print(tile?.image);
    } else {
      velocity *= -1.0;
      bounce[rand.nextInt(2)].play();
      return move(meta, atlas);
    }
    if (tile == null) {
      gotoTarget(target);
    } else if (tile.image == "wall") {
      velocity *= -1.0;
      bounce[rand.nextInt(2)].play();
      return move(meta, atlas);
    } else if (tile.image == "togglable_block") {
      velocity *= -1.0;
      bounce[rand.nextInt(2)].play();
      return move(meta, atlas);
    } else if (tile.image.contains("oneway")) {
      if (direction(tile.image) == direction(velocity)) {
        gotoTarget(target);
      } else {
        velocity *= -1.0;
        bounce[rand.nextInt(2)].play();
        return move(meta, atlas);
      }
    } else if (tile.image.contains("ice")) {
      if (tile.image.contains("weakened")) {
        tile.image = "floor";
        tile.texture = atlas["floor"];
        breakIce.play();
        gotoTarget(target);
      } else {
        tile.image = "ice_weakened";
        tile.texture = atlas["ice_weakened"];
        velocity *= -1.0;
        crackIce.play();
        return move(meta, atlas);
      }
    } else if (tile.image.contains("portal") && tile.image.contains("enter")) {
      if (direction(tile.image) == direction(velocity)) {
        Vector2 exit = findExit(meta);
        if(!(exit.x == -1.0)) {
          position = exit.clone();
          print(meta.getTile(exit.x, exit.y));
          velocity = velocityFor(direction(meta
              .getTile(exit.x, exit.y)
              .image));
          portal.play();
          return move(meta, atlas);
        } else {
          gotoTarget(target);
        }
      } else {
        gotoTarget(target);
      }
    } else if(tile.image.contains("switch")) {
      if (tile.image.contains("off")) {
        tile.image = "switch_on";
        tile.texture = atlas["switch_on"];
        for(BasicTile tile in findDisappearingBlocks(meta)) {
          if(tile.image.contains("block")) {
            tile.image = "togglable_empty";
            tile.texture = atlas["togglable_empty"];
          } else {
            tile.image = "togglable_block";
            tile.texture = atlas["togglable_block"];
          }
        }
        switchFlip.play();
        gotoTarget(target);
      } else {
        tile.image = "switch_off";
        tile.texture = atlas["switch_off"];
        for(BasicTile tile in findDisappearingBlocks(meta)) {
          if(tile.image.contains("block")) {
            tile.image = "togglable_empty";
            tile.texture = atlas["togglable_empty"];
          } else {
            tile.image = "togglable_block";
            tile.texture = atlas["togglable_block"];
          }
        }
        switchFlip.play();
        gotoTarget(target);
      }
    } else if(tile.image.contains("ramp")) {
        if(tile.image.contains("top")) {
          if(velocity.y < 0) {
            velocity *= -1.0;
            bounce[rand.nextInt(2)].play();
            return move(meta, atlas);
          }
          gotoTarget(target);
        }
        if(tile.image.contains("bottom")) {
          if(velocity.y > 0) {
            velocity *= -1.0;
            bounce[rand.nextInt(2)].play();
            return move(meta, atlas);
          }
          gotoTarget(target);
        }
        if(tile.image.contains("left")) {
          if(velocity.x > 0) {
            velocity *= -1.0;
            bounce[rand.nextInt(2)].play();
            return move(meta, atlas);
          }
          gotoTarget(target);
        }
        if(tile.image.contains("right")) {
          if(velocity.x < 0) {
            velocity *= -1.0;
            bounce[rand.nextInt(2)].play();
            return move(meta, atlas);
          }
          gotoTarget(target);
        }
    } else if(tile.image == "goal") {
      win.play();
      return true;
    } else {
      gotoTarget(target);
    }
    return false;
  }

  Vector2 findExit(EditableTileLayer meta) {
    for (int i = 0; i < meta.width; i++) {
      for (int j = 0; j < meta.height; j++) {
        if (meta.getTile(i, j) != null &&
            meta.getTile(i, j).image.contains("exit")) {
          return new Vector2(i.toDouble(), j.toDouble());
        }
      }
    }
    return new Vector2.all(-1.0);
  }

  List<BasicTile> findDisappearingBlocks(EditableTileLayer meta) {
    List<BasicTile> blocks = [];
    for (int i = 0; i < meta.width; i++) {
      for (int j = 0; j < meta.height; j++) {
        if (meta.getTile(i, j) != null &&
            meta.getTile(i, j).image.contains("togglable")) {
          blocks.add(meta.getTile(i, j));
        }
      }
    }
    return blocks;
  }

  void gotoTarget(Vector2 target) {
    new Tween.to(position, Vector2Accessor.XY, 0.08)
      ..targetValues = [target.x, target.y]
      ..easing = TweenEquations.easeNone
      ..start(tweenManager);
  }

  int direction(var thing) {
    if (thing is Vector2) {
      if (thing.y > 0) {
        return 1;
      }
      if (thing.x > 0) {
        return 2;
      }
      if (thing.y < 0) {
        return 3;
      }
      if (thing.x < 0) {
        return 4;
      }
    } else if (thing is String) {
      if (thing.contains("up")) {
        return 1;
      }
      if (thing.contains("right")) {
        return 2;
      }
      if (thing.contains("down")) {
        return 3;
      }
      if (thing.contains("left")) {
        return 4;
      }
    }
    return 0;
  }

  Vector2 velocityFor(int direction) {
    switch (direction) {
      case 1:
        return new Vector2(0.0, 1.0);
      case 2:
        return new Vector2(1.0, 0.0);
      case 3:
        return new Vector2(0.0, -1.0);
      case 4:
        return new Vector2(-1.0, 0.0);
      default:
        print("ERROR: NOT A DIRECTION");
        return new Vector2.zero();
    }
  }

  render(SpriteBatch batch, num xOffset, num yOffset) {
    batch.draw(texture, position.x * 64 + xOffset, position.y * 64 + yOffset);
  }
}
