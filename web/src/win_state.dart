part of game;

class WinState extends State {

  SpriteBatch batch;
  Camera2D fullscreen;

  bool created = false;

  Music won;

  double timeAngle = 0.0;

  @override
  create() {
    batch = new SpriteBatch(assetManager.get("default"));
    fullscreen = new Camera2D.originBottomLeft(width, height);
    created = true;

    querySelector("#levelselect").hidden = true;
    querySelector("#win").hidden = false;
    querySelector("#winmessage").style.opacity = "1.0";

    won = assetManager.get("won");
    won.loop();
  }

  @override
  preload() {

  }

  @override
  render(num delta) {
    fullscreen.update();
    batch.projection = fullscreen.combined;

    double minDim = min(width.toDouble(), height.toDouble());
    double maxDim = max(width.toDouble(), height.toDouble()) * 2;

    batch.begin();
    batch.draw(atlas["galaxy"], width / 2 - maxDim / 2, height / 2 - maxDim / 2 - minDim,
        width: maxDim, height: maxDim, angle: timeAngle);
    batch.end();
  }

  @override
  update(num delta) {
    if(!created) {
      create();
    }
    timeAngle += delta * 0.5;
    if(timeAngle >= radians(360.0)) {
      timeAngle = 0.0;
    }
  }

  @override
  resize(num width, num height) {
    fullscreen = new Camera2D.originBottomLeft(width, height);
  }

  @override
  pause() {

  }

  @override
  resume() {

  }
}