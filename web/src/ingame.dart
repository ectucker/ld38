part of game;

class IngameState extends State {

  SpriteBatch batch;
  Camera2D fullscreen;

  Framebuffer lighting;
  Framebuffer pass1;
  Framebuffer pass2;

  Map<String, Texture> atlas;
  Texture lightmap;

  var yaml;
  int level;

  int playerX = 100;
  bool left, right = false;

  double time = 0.5;

  List<StreamSubscription> subscriptions = [];

  IngameState(this.yaml, this.level): super() {

  }

  @override
  create() {
    batch = new SpriteBatch(assetManager.get("lighting"));
    atlas = assetManager.get("atlas");
    lightmap = assetManager.get("lightmap");
    fullscreen = new Camera2D.originBottomLeft(width, height);

    querySelector("#main").hidden = true;
    querySelector("#hud").hidden = false;

    pass1 = new Framebuffer(width, height, shader: assetManager.get("hBlur"));
    pass2 = new Framebuffer(width, height, shader: assetManager.get("vBlur"));
    lighting = new Framebuffer(width, height, shader: assetManager.get("lighting"));
  }

  nextLevel() {
    game.pushState(new IngameState(yaml, level + 1));
  }

  @override
  preload() {
    assetManager.load("atlas", loadAtlas("assets/atlas.json", loadTexture("assets/atlas.png")));
    assetManager.load("hBlur", loadProgram("packages/cobblestone/shaders/batch.vertex", "assets/glsl/hBlur.fragment"));
    assetManager.load("vBlur", loadProgram("packages/cobblestone/shaders/batch.vertex", "assets/glsl/vBlur.fragment"));
    assetManager.load("lighting", loadProgram("packages/cobblestone/shaders/batch.vertex", "assets/glsl/time.fragment"));
    assetManager.load("lightmap", loadTexture("assets/timecycle.png", mipMap));
  }

  @override
  render(num delta) {
    fullscreen.update();
    batch.projection = fullscreen.combined;

    lighting.beginCapture();
    clearScreen(Colors.skyBlue);
    batch.begin();
    batch.draw(atlas["island"], 0, 0);
    batch.draw(atlas["man"], playerX, 500);
    batch.end();
    lighting.endCapture();

    lighting.shader.startProgram();
    lightmap.bind(1);
    lighting.setUniform("uLightmap", 1);
    lighting.setUniform("uTime", time);
    lighting.render(fullscreen.combined, 0, 0, width, height);
  }

  @override
  update(num delta) {
    if(left) {
      playerX -= delta * 100;
    }
    if(right) {
      playerX += delta * 100;
    }

    time += delta;
    if(time > 1.0) {
      time = 0.0;
    }
  }

  @override
  resize(num width, num height) {
    fullscreen = new Camera2D.originBottomLeft(width, height);
    pass1 = new Framebuffer(width, height, shader: assetManager.get("hBlur"));
    pass2 = new Framebuffer(width, height, shader: assetManager.get("vBlur"));
    lighting = new Framebuffer(width, height, shader: assetManager.get("lighting"));
  }

  void onKeyDown(KeyboardEvent e) {
    if(e.keyCode == KeyCode.LEFT) {
      left = true;
    }
    if(e.keyCode == KeyCode.RIGHT) {
      right = true;
    }
  }

  void onKeyUp(KeyboardEvent e) {
    if(e.keyCode == KeyCode.LEFT) {
      left = false;
    }
    if(e.keyCode == KeyCode.RIGHT) {
      right = false;
    }
  }

  @override
  pause() {
    subscriptions.forEach((s) => s.cancel());
  }

  @override
  resume() {
    subscriptions.add(window.onKeyDown.listen(onKeyDown));
    subscriptions.add(window.onKeyUp.listen(onKeyUp));
  }
}