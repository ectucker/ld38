part of game;

Map<String, Texture> atlas;

class Game extends BaseGame {

  State state;
  State push = null;

  @override
  create() {
    atlas = assetManager.get("atlas");
    setGLViewport(canvasWidth, canvasHeight);
    pushState(new LevelSelect());
  }

  pushState(State state) {
    this.state?.pause();
    push = state;
    push.preload();
  }

  @override
  preload() {
    assetManager.load("atlas", loadAtlas("assets/atlas.json", loadTexture("assets/atlas.png")));
    assetManager.load("click", loadSound("assets/sound/click.ogg"));
    assetManager.load("portal", loadSound("assets/sound/portal.ogg"));
    assetManager.load("crack", loadSound("assets/sound/crack.ogg"));
    assetManager.load("break", loadSound("assets/sound/break.ogg"));
    assetManager.load("bounce", loadSound("assets/sound/bounce.ogg"));
    assetManager.load("bounce2", loadSound("assets/sound/bounce2.ogg"));
    assetManager.load("win", loadSound("assets/sound/win.ogg"));
    assetManager.load("switch", loadSound("assets/sound/switch.ogg"));
    assetManager.load("whoosh", loadSound("assets/sound/whoosh.ogg"));
    assetManager.load("menu", loadMusic("assets/sound/menu.ogg"));
    assetManager.load("music_water", loadMusic("assets/sound/music_water.ogg"));
    assetManager.load("music_desert", loadMusic("assets/sound/music_desert.ogg"));
    assetManager.load("music_forest", loadMusic("assets/sound/music_forest.ogg"));
    assetManager.load("final", loadMusic("assets/sound/final.ogg"));
    assetManager.load("won", loadMusic("assets/sound/won.ogg"));
    assetManager.load("music_ice", loadMusic("assets/sound/music_ice.ogg"));
  }

  @override
  render(num delta) {
    clearScreen(Colors.black);

    state?.render(delta);
  }

  @override
  update(num delta) {
    if(assetManager.allLoaded()) {
      push?.create();
      push?.resume();
      state = push ?? state;
      push = null;
    }

    state?.update(delta);
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

  @override
  resize(num width, num height) {
    setGLViewport(canvasWidth, canvasHeight);
    state?.resize(width, height);
  }

  @override
  pause() {
    state?.pause();
  }

  @override
  resume() {
    state?.resume();
  }

}