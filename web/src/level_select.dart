part of game;

List<int> completeLevels = [];

class LevelSelect extends State {
  SpriteBatch batch;
  Camera2D fullscreen;
  List<StreamSubscription> subscriptions = [];

  Map<int, double> openPaths = {};
  Vector4 things = new Vector4.zero();

  double timeAngle = 0.0;

  Map levelList;
  List availableLevels;

  int selectedLevel = 0;

  Sound whoosh;
  Music menu;

  @override
  create() {
    batch = new SpriteBatch(assetManager.get("default"));
    fullscreen = new Camera2D.originBottomLeft(width, height);

    levelList = loadYaml(assetManager.get("levellist"));
    availableLevels = findAvailableLevels();

    querySelector("#loading").hidden = true;
    querySelector("#levelselect").hidden = false;
    querySelector("#hud").hidden = true;

    things.x = openPaths[availableLevels[selectedLevel]["id"]];
    things.y = min(width.toDouble(), height.toDouble()) / 6;

    menu = assetManager.get("menu");
    menu.loop();
    whoosh = assetManager.get("whoosh");

    print(completeLevels);
    if(completeLevels.contains(26)) {
      menu.stop();
      game.pushState(new WinState());
    }
  }

  List findAvailableLevels() {
    openPaths = {};
    List levels = [];
    levelList["levels"].forEach((Map value) {
      int key = value.keys.first;
      if (!completeLevels.contains(key)) {
        if (value[key]["prereqs"] != null) {
          for (int i = 0; i < value[key]["prereqs"].length; i++) {
            var list = value[key]["prereqs"][i];
            if (list is List) {
              bool available = true;
              for (int item in list) {
                if (!completeLevels.contains(item)) {
                  available = false;
                }
              }
              if (available && !levels.contains(value)) {
                Map allData = new Map.from(value[key]);
                allData["id"] = key;
                levels.add(allData);
                openPaths[key] = value[key]["prereqs"][i + 1];
              }
            }
          }
        } else {
          Map allData = new Map.from(value[key]);
          allData["id"] = key;
          levels.add(allData);
        }
      }
    });
    return levels;
  }

  @override
  preload() {
    assetManager.load(
        "default",
        loadProgram("packages/cobblestone/shaders/batch.vertex",
            "assets/glsl/ditchtrans.fragment"));
    assetManager.load(
        "levellist", HttpRequest.getString("assets/levellist.yml"));
  }

  @override
  render(num delta) {
    fullscreen.update();
    batch.projection = fullscreen.combined;

    double minDim = min(width.toDouble(), height.toDouble());
    double maxDim = max(width.toDouble(), height.toDouble()) * 2;

    clearScreen(Colors.midnightBlue);
    batch.begin();
    batch.draw(atlas["galaxy"], width / 2 - maxDim / 2, height / 2 - maxDim / 2 - minDim,
        width: maxDim, height: maxDim, angle: timeAngle + radians(things.x));
    for (int i = 1; i <= 26; i++) {
      if (i != 12) {
        if (!completeLevels.contains(i)) {
          if (availableLevels[selectedLevel]["id"] == i) {
            batch.draw(atlas["world" + i.toString()], width / 2 - minDim / 2,
                -0.5 * minDim + things.y,
                angle: radians(things.x), width: minDim, height: minDim);
          } else if (things.z == i) {
            batch.draw(atlas["world" + i.toString()], width / 2 - minDim / 2,
                -0.5 * minDim + things.w,
                angle: radians(things.x), width: minDim, height: minDim);
          } else {
            batch.draw(atlas["world" + i.toString()], width / 2 - minDim / 2,
                -0.5 * minDim,
                angle: radians(things.x), width: minDim, height: minDim);
          }
        }
      }
    }
    batch.end();

  }

  @override
  update(num delta) {
    timeAngle += delta * 0.5;
    if(timeAngle >= radians(360.0)) {
      timeAngle = 0.0;
    }
  }

  @override
  resize(num width, num height) {
    fullscreen = new Camera2D.originBottomLeft(width, height);
  }

  @override
  pause() {
    subscriptions.forEach((s) => s.cancel());
    menu?.stop();
  }

  @override
  resume() {
    subscriptions.add(document.onKeyDown.listen(keyPress));
  }

  keyPress(KeyboardEvent e) async {
    if (e.keyCode == KeyCode.DOWN) {
      things.z = availableLevels[selectedLevel]["id"];
      selectedLevel--;
      if (selectedLevel < 0) {
        selectedLevel = availableLevels.length - 1;
      }

      //print(things);
      //print(openPaths[availableLevels[selectedLevel]["id"]]);
      //print(width);
      //print(height);
      new Tween.to(things, Vector4Accessor.X, 0.5)
        ..targetValues = [openPaths[availableLevels[selectedLevel]["id"]]]
        ..easing = TweenEquations.easeInOutCirc
        ..start(tweenManager);
      things.w = things.y;
      new Tween.to(things, Vector4Accessor.W, 0.5)
        ..targetValues = [0.0]
        ..easing = TweenEquations.easeInOutCubic
        ..start(tweenManager);
      things.y = 0.0;
      new Tween.to(things, Vector4Accessor.Y, 0.5)
        ..targetValues = [min(width.toDouble(), height.toDouble()) / 6]
        ..easing = TweenEquations.easeInOutCubic
        ..start(tweenManager);
      whoosh.play();
    }
    if (e.keyCode == KeyCode.UP) {
      things.z = availableLevels[selectedLevel]["id"];
      selectedLevel++;
      if (selectedLevel >= availableLevels.length) {
        selectedLevel = 0;
      }
      //print(things);
      //print(openPaths[availableLevels[selectedLevel]["id"]]);
      //print(width);
      //print(height);
      new Tween.to(things, Vector4Accessor.X, 0.5)
        ..targetValues = [openPaths[availableLevels[selectedLevel]["id"]]]
        ..easing = TweenEquations.easeInOutCirc
        ..start(tweenManager);
      things.w = things.y;
      new Tween.to(things, Vector4Accessor.W, 0.5)
        ..targetValues = [0]
        ..easing = TweenEquations.easeInOutCubic
        ..start(tweenManager);
      things.y = 0.0;
      new Tween.to(things, Vector4Accessor.Y, 0.5)
        ..targetValues = [min(width.toDouble(), height.toDouble()) / 6]
        ..easing = TweenEquations.easeInOutCubic
        ..start(tweenManager);
      whoosh.play();
    }
    if (e.keyCode == KeyCode.ENTER || e.keyCode == KeyCode.SPACE) {
      game.pushState(new PuzzleState(
          await loadEditableTilemap(
              "assets/maps/" + availableLevels[selectedLevel]["file"], atlas),
          assetManager.get(availableLevels[selectedLevel]["music"]),
          availableLevels[selectedLevel]["id"]));
      pause();
    }
    if (e.key == "u") {
      availableLevels = [];
      levelList["levels"].forEach((Map value) {
        int key = value.keys.first;
        Map allData = new Map.from(value[key]);
        allData["id"] = key;
        availableLevels.add(allData);
        openPaths[key] = 180.0;
      });
    }
  }
}
