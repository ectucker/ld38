library game;

import 'dart:convert';
import 'package:cobblestone/cobblestone.dart';
import 'package:tweenengine/tweenengine.dart';
import 'package:tweenengine/tweenengine.dart';
import 'package:yaml/yaml.dart';
import 'package:path/path.dart' as Path;

part 'game.dart';
part 'ingame.dart';
part 'puzzlestate.dart';
part 'ball.dart';
part 'editable_tilemap.dart';
part 'level_select.dart';
part 'win_state.dart';

Game game;

void main() {
  game = new Game();
}